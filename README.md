Newlisp For The Win
===================

This repository contain miscellaneous newlisp scripts that are to be
used in isolation or together as appropriate.

You might need to install newlisp:

    # apt-get install newlisp

These scripts are published with NO WARRANTY THAT THEY WORK.
