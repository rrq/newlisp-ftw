#!/usr/bin/newlisp
#
# This script undo the "merged usr" directories; i.e., replace any
# toplevel links /bin, /sbin, /lib, /lib32, /libx32, or /lib64 with
# proper directories of their content according to installation paths.
#
# 

(signal 2 (fn (x) (exit 1)))

(define (replace-file A B)
  (delete-file A)
  (rename-file B A))

(define (installs-as PATH)
  (= (! (format "/usr/bin/dpkg -S \"%s\" > /dev/null 2>&1" PATH))))

(define (delete-file-exist PATH)
  (and (file? PATH) (delete-file PATH)))

(setf UNSURE "/var/log/UNSURE" TELL true)
(define (unsure PATH)
  (when TELL
    (println (format "Adding paths to %s with unsure pathnames" UNSURE))
    (setf TELL nil)
    )
  (append-file UNSURE (string PATH "\n")))

(define (delete-empty-dirs D)
  (! (format "/usr/bin/find %s /usr%s -depth -type d -delete 2>/dev/null" D D))
  )

#### Un-merging process starts here
(setf DIRS '("/bin" "/sbin" "/lib" "/lib32" "/libx32" "/lib64")
      CP (if (file? "/bin/cp") "/bin/cp" "/usr/bin/cp") )

(println (date))

# Make target directories as copies of their /usr linking
(dolist (D DIRS)
  (when (and (= 41471 (file-info D 1)) (directory? D))
    (println "copying " D)
    (let ((Dx (format "%s-tobe" D)))
      (and (make-dir Dx)
           (= (! (format "%s -a /usr%s/. %s/." CP D Dx)))
           (replace-file D Dx)))))

# Rebuild library cache
(= (! "/sbin/ldconfig"))

# Remove counterpart of known installed file(s), then remove any empty
# directories
(dolist (D DIRS)
  (when (directory? (string "/usr" D))
    (let ((N 0) (FILES (exec (format "/usr/bin/find /usr%s -type f" D))))
      (println "checking " (length FILES) " files for " D)
      (dolist (F FILES)
        (when (= (% (inc N) 100))
          (print (format "checked %d of %d files for %s\r"
                         N (length FILES) D))
          (setf TELL true))
        (if (installs-as F) (delete-file-exist (4 F))
          (installs-as (4 F)) (delete-file F)
          (unsure F)))
      (delete-empty-dirs D)
      )
    (println)
    ))

# Rebuild library cache
(= (! "/sbin/ldconfig"))

# Ensure the root directories still/again exist 
(dolist (D DIRS)
  (! (format "/bin/mkdir -p %s /usr%s" D D)))

(print "Push enter to reboot: ")
(read-line)
(! "/usr/sbin/reboot")
(exit 0)
