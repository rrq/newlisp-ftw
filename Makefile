MANPAGES = arper.lsp.8
HTMLPAGES = $(MANPAGES:%.lsp.8=%.html)

default: $(MANPAGES)

$(MANPAGES): %: %.adoc
	a2x -d manpage -f manpage $^


$(HTMLPAGES): %.html: %.adoc
	asciidoc -bhtml $^
