#!/usr/bin/newlisp

# libc6 library
(constant 'LIB "/lib/x86_64-linux-gnu/libc.so.6")

(import LIB "ioctl" "int"
        "int" ; fd
        "unsigned long" ; request
        "void*" ; data [optional]
        )

(constant
 'USBDEVFS_RESET 21780  ; _IO('U', 20)
 )

(define (die)
  (write-line 2 (join (map string (args)) " "))
  (exit 0))

(unless (setf FD (open (main-args -1) "write"))
  (die "cannot open" (main-args -1)))

(println (ioctl FD USBDEVFS_RESET 0))
(exit 0)
